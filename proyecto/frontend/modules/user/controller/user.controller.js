app.controller('menuCtrl', function ($scope, $uibModal, UserService, $rootScope) {
    UserService.login();
    $rootScope.bannerV = false;
    $rootScope.bannerText = "";


    $scope.open = function () {

        var modalInstance = $uibModal.open({
            animation: 'true',
            templateUrl: 'frontend/modules/user/view/modal.view.html',
            controller: 'modalWindowCtrl',
            size: "lg"
        });
    };

    $scope.logout = function () {

        UserService.logout();
    };
});

app.controller('modalWindowCtrl', function ($scope, $uibModalInstance, services,
        CommonService, $cookies, $location, UserService, twitterService, facebookService) {

    twitterService.initialize();
    $scope.form = {
        user: "",
        pass: ""
    };
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.login = function () {
        $scope.err = false;
        var data = {"usuario": $scope.form.user, "pass": $scope.form.pass};
        data = JSON.stringify(data);
        services.post("user", "login", data).then(function (response) {
            if (!response.data.error) {



                $cookies.putObject("session", {usuario: response.data[0]['usuario'], avatar: response.data[0]['avatar'], tipo: response.data[0]['tipo'], nombre: response.data[0]['nombre']}, {expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)});
                //   window.location.href=CommonService.amigable('?pr=JoinElderly_project&cr=proyecto&angular=#');
                $scope.close();
                UserService.login();
                // FlashService.Error("Error, intentelo mas tarde", false);
            } else {
                if (response.data.datos == 503)
                    // window.location.href = amigable("?module=main&fn=begin&param=503");
                    CommonService.banner("Error, intentelo mas tarde", "Err");
                else if (response.data.error == 404)
                    $location.path("/");
                else {
                    $scope.errorpass = response.data.datos;
                    $scope.err = true;
                }
            }
        });
    };
    $scope.loginTw = function () {
        twitterService.connectTwitter().then(function () {
            //    console.log(twitterService.isReady());
            if (twitterService.isReady()) {
                twitterService.getUserInfo().then(function (data) {
                    services.post("user", 'social_signin', {id: data.id, nombre: data.name, avatar: data.profile_image_url, twitter: true}).then(function (response) {

                        if (!response.data.error) {



                            $cookies.putObject("session", {usuario: response.data[0]['usuario'], avatar: response.data[0]['avatar'], tipo: response.data[0]['tipo'], nombre: response.data[0]['nombre']}, {expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)});
                            //   window.location.href=CommonService.amigable('?pr=JoinElderly_project&cr=proyecto&angular=#');
                            $scope.close();
                            UserService.login();
                            // FlashService.Error("Error, intentelo mas tarde", false);
                        } else {
                            if (response.data.datos == 503)
                                // window.location.href = amigable("?module=main&fn=begin&param=503");
                                CommonService.banner("Error, intentelo mas tarde", "Err");
                            else if (response.data.error == 404)
                                $location.path("/");

                        }
                    });
                });
            }

        });


    };
    $scope.loginFb = function () {
        facebookService.login().then(function () {
            facebookService.me().then(function (user) {

                if (user.error)
                    $scope.close();
                else
                    services.post("user", 'social_signin', {id: user.id, nombre: user.first_name, apellidos: user.last_name, email: user.email}).then(function (response) {

                        if (!response.data.error) {

                            $cookies.putObject("session", {usuario: response.data[0]['usuario'], avatar: response.data[0]['avatar'], tipo: response.data[0]['tipo'], nombre: response.data[0]['nombre']}, {expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)});

                            $scope.close();
                            UserService.login();

                        } else {
                            if (response.data.datos == 503)
                                // window.location.href = amigable("?module=main&fn=begin&param=503");
                                CommonService.banner("Error, intentelo mas tarde", "Err");
                            else if (response.data.error == 404)
                                $location.path("/");

                        }

                    });
            });
        });
    };

});
app.controller('profileCtrl', function ($scope, UserService, services, user, $location, $cookies, CommonService) {
    $scope.admin = false;
    if (!user)
        $location.path("/");
    if (user.data.usuario !== $cookies.getObject('session').usuario && $cookies.getObject('session').tipo != 'admin')
        $location.path("/");
    else if (user.data.usuario !== $cookies.getObject('session').usuario)
        $scope.admin = true;

    user.data.user.password = "";
    $scope.user = user.data.user;
    $scope.drop = {
        msgClass: ''
    };

    if (!isNaN(user.data.user.usuario))
        $scope.user.usuario = user.data.user.nombre;

    $scope.controlmail = false;
    $scope.controldni = false;

    if (user.data.user.email)
        $scope.controlmail = true;
    if (user.data.user.dni)
        $scope.controldni = true;



    UserService.loadPais(user.data.user.pais);
    // $("#pais").append('<option value=" " selected="selected">' + $scope.user.pais + '</option>');

    if (user.data.user.pais == 'ES') {
        UserService.loadProvincia(user.data.user.provincia);
        //  $("#provincia").append('<option value=" " selected="selected">' + $scope.user.provincia + '</option>');
        if (user.data.user.provincia !== '' && user.data.user.provincia !== ' ') {
            UserService.loadPoblacion(user.data.user.provincia, user.data.user.poblacion);
            //  $("#poblacion").append('<option value=" " selected="selected">' + $scope.user.poblacion + '</option>');
        }
    }

    $scope.submit = function () {

        var data = JSON.stringify($scope.user);
        //console.log(data);
        services.put("user", "modify", data).then(function (response) {
            if (response.data.success) {
                if (!$scope.admin) {
                    $cookies.putObject("session", {usuario: $scope.user.usuario, avatar: $scope.user.avatar, tipo: $scope.user.tipo, nombre: $scope.user.nombre}, {expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)});

                    UserService.login();
                    $location.path($location.path());
                    CommonService.banner("Su perfil ha sido modificado satisfactoriamente", "");
                } else {
                    $location.path('/admin/list');
                }
                // FlashService.Error("Error, intentelo mas tarde", false);
            } else {
                if (response.data.datos == 503)
                    // window.location.href = amigable("?module=main&fn=begin&param=503");
                    CommonService.banner("Error, intentelo mas tarde", "Err");
                else if (response.data.error == 404)
                    $location.path("/");
                else {
                    //
                }
            }
        });
    };

    $scope.dropzoneConfig = {
        'options': {// passed into the Dropzone constructor
            'url': 'backend/index.php?module=user&function=upload_avatar',
            addRemoveLinks: true
        },
        'eventHandlers': {
            'sending': function (file, formData, xhr) {
            },
            'success': function (file, response) {


                response = JSON.parse(response);
                if (response.resultado) {


                    $(".msg").addClass('msg_ok').removeClass('msg_error').text('Success Upload image!!');

                    $('.msg').animate({'right': '300px'}, 300);

                    $cookies.putObject("session", {usuario: $scope.user.usuario, avatar: response.datos, tipo: $scope.user.tipo, nombre: $scope.user.nombre}, {expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)});
                    $scope.user.avatar = response.datos;
                    UserService.login();

                } else {

                    $(".msg").addClass('msg_error').removeClass('msg_ok').text(response['error']);

                    $('.msg').animate({'right': '300px'}, 300);

                }
            },
            'removedfile': function (file, serverFileName) {
                if (file.xhr.response) {

                    var data = jQuery.parseJSON(file.xhr.response);

                    services.post("user", "delete_avatar", JSON.stringify({'filename': data}));

                }
            }
        }};

    $scope.resetValues = function () {
        UserService.loadPoblacion(user.data.user.provincia, user.data.user.poblacion);
    };
    $scope.resetPais = function () {
        if (user.data.user.pais == 'ES') {
            UserService.loadProvincia(user.data.user.pais);
            UserService.loadPoblacion(user.data.user.provincia, user.data.user.poblacion);
        } else {
            $("#provincia").empty();
            $("#poblacion").empty();
        }
    };
});

app.controller('verifyCtrl', function ($cookies, UserService, $location, CommonService, $route, services) {

    var token = $route.current.params.token;
    if (token.substring(0, 3) !== 'Ver') {
        CommonService.banner("Ha habido algún tipo de error con la dirección", "Err");
        $location.path('/');

    }
    services.get("user", "activar", 3, token).then(function (response) {

        if (response.data.success) {
            CommonService.banner("Su cuenta ha sido satisfactoriamente verificada", "");
            $cookies.putObject("session", {usuario: response.data.user[0]['usuario'], avatar: response.data.user[0]['avatar'], tipo: response.data.user[0]['tipo'], nombre: response.data.user[0]['nombre']}, {expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)});
            UserService.login();
            $location.path('/');

        } else {
            if (response.data.datos == 503)
                // window.location.href = amigable("?module=main&fn=begin&param=503");
                CommonService.banner("Error, intentelo mas tarde", "Err");
            else if (response.data.error == 404)
                $location.path("/");
            else {
                //
            }
        }
    });
});

app.controller('changepassCtrl', function ($route, $scope, services, $location, CommonService) {

    $scope.token = $route.current.params.token;

    $scope.changepass = {
        inputPassword: ""
    };

    $scope.SubmitChangePass = function () {

        var data = {"password": $scope.changepass.inputPassword, "token": $scope.token};
        var passw = JSON.stringify(data);

        services.put('user', 'update_pass', passw).then(function (response) {

            if (response.data.success) {
                $location.path('/');
                CommonService.banner("Tu contraseña se ha cambiado correctamente", "");
            } else {
                CommonService.banner("Error en el servidor", "Err");
            }
        });
    };
});


app.controller('restoreCtrl', function ($scope, services, $timeout, $location, CommonService) {

    $scope.restore = {
        inputEmail: ""
    };

    $('.modal').remove();
    $('.modal-backdrop').remove();
    $("body").removeClass("modal-open");

    $scope.SubmitRestore = function () {

        var data = {"inputEmail": $scope.restore.inputEmail, "token": 'restore_form'};
        var restore_form = JSON.stringify(data);

        services.post('user', 'process_restore', restore_form).then(function (response) {

            response.data = response.data.split("|");
            $scope.message = response.data[1];

            if (response.data[0].substring(2, 6) == 'true') {

                $scope.class = 'alert alert-success';

                $timeout(function () {
                    $location.path('/');
                    CommonService.banner("Revisa la bandeja de tu correo", "");
                }, 3000);

            } else {
                $scope.class = 'alert alert-error';
            }

        });
    };
});

app.controller('signupCtrl', function ($scope, services, $location, $timeout, CommonService) {

    $scope.signup = {
        inputUser: "",
        inputName: "",
        inputSurn: "",
        inputEmail: "",
        inputPass: "",
        inputPass2: "",
        inputBirth: "",
        inputType: "client",
        inputBank: "",
        inputDni: ""
    };

    $('.modal').remove();
    $('.modal-backdrop').remove();
    $("body").removeClass("modal-open");


    $scope.SubmitSignUp = function () {

        var data = {"usuario": $scope.signup.inputUser, "nombre": $scope.signup.inputName, "apellidos": $scope.signup.inputSurn, "email": $scope.signup.inputEmail,
            "password": $scope.signup.inputPass, "password2": $scope.signup.inputPass2, "date_birthday": $scope.signup.inputBirth, "tipo": $scope.signup.inputType,
            "bank": $scope.signup.inputBank, "dni": $scope.signup.inputDni};

        var data_users_JSON = JSON.stringify(data);

        services.post('user', 'signup_user', data_users_JSON).then(function (response) {

            if (response.data.success) {
                $timeout(function () {
                    $location.path('/');
                    CommonService.banner("El usuario se ha dado de alta correctamente, revisa su correo para activarlo", "");
                }, 2000);
            } else {
                if (response.data.typeErr === "Name") {
                    $("#inputUser").focus().after("<span class='error'>" + response.data.error + "</span>");

                } else if (response.data.typeErr === "Email") {
                    $("#inputEmail").focus().after("<span class='error'>" + response.data.error + "</span>");
                }

                else {
                    CommonService.banner("Error en el servidor", "Err");
                }
            }
        });
    };
});
