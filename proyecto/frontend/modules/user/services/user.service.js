app.factory("UserService", ['$cookies', '$location', '$rootScope', 'services','facebookService', function ($cookies, $location, $rootScope, services,facebookService) {
        var service = {};
        service.login = login;
        service.logout = logout;
        service.loadPais = loadPais;
        service.loadProvincia = loadProvincia;
        service.loadPoblacion = loadPoblacion;
        return service;

        function login() {

            var user = $cookies.getObject("session");
            if (user) {
                $rootScope.accederV = false;

                $rootScope.profileV = true;
                $rootScope.logoutV = true;

                $rootScope.avatar = user.avatar;
                $rootScope.nombre = user.nombre;

                if (user.tipo === "worker") {
                    $rootScope.adminV = false;
                    $rootScope.misofertasV = true;
                } else if (user.tipo === "admin") {
                    $rootScope.adminV = true;
                    $rootScope.misofertasV = false;
                } else {
                    $rootScope.adminV = false;
                    $rootScope.misofertasV = false;
                }

            } else {
                $rootScope.accederV = true;
            }
        }

        function logout() {
            
            facebookService.logout();
            
            $cookies.remove("session");
            $rootScope.accederV = true;
            $rootScope.profileV = false;

            $rootScope.avatar = '';
            $rootScope.nombre = '';

            $rootScope.adminV = false;
            $rootScope.misofertasV = false;

            $rootScope.logoutV = false;
            $location.path("/");
        }

        function loadPais(pais) {
            services.get("user", "load_pais_user", 3, true).then(function (data) {

                if (data.data === 'error') {
                    loadPaisv2("backend/resources/ListOfCountryNamesByName.json", pais);
                } else {
                    //$scope.user.pais = user.pais;
                    loadPaisv2("backend?index.php&module=user&function=load_pais_user&param=true", pais);

                }
            });
        }
        function loadPaisv2(cad, pais) {
            $.getJSON(cad, function (data) {
                $("#pais").empty();
                if (!pais)
                    $("#pais").append('<option value="" selected="selected">Selecciona un Pais</option>');

                $.each(data, function (i, valor) {
                    if (valor.sName.length > 20)
                        valor.sName = valor.sName.substring(0, 19);
                    if (pais == valor.sISOCode)
                        $("#pais").append("<option value='" + valor.sISOCode + "' selected='selected' >" + valor.sName + "</option>");
                    else
                        $("#pais").append("<option value='" + valor.sISOCode + "'>" + valor.sName + "</option>");

                });
            })
                    .fail(function () {
                        alert("error load_countries");
                    });
        }
        function loadProvincia(prov) {
            services.get("user", "load_provincias_user", 3, true).then(function (response) {

                $("#provincia").empty();
                var provincias = response.data.provincias;
                
                if (provincias === 'error') {
                    //
                } else {
                    //  var json = JSON.parse(data.data);
                    for (var i = 0; i < provincias.length; i++) {
                        if (prov == provincias[i].id)
                            $("#provincia").append("<option value='" + provincias[i].id + "' selected='selected'>" + provincias[i].nombre + "</option>");
                        else
                            $("#provincia").append("<option value='" + provincias[i].id + "'>" + provincias[i].nombre + "</option>");
                    }
                }
            });
        }
        function loadPoblacion(prov, pobl) {
            var datos = {idPoblac: prov};

            services.post("user", "load_poblaciones_user", datos).then(function (response) {

                var poblaciones = response.data.poblaciones;

                $("#poblacion").empty();
                // $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');

                if (poblaciones === 'error') {
                    //
                } else {
                    for (var i = 0; i < poblaciones.length; i++) {
                        if (poblaciones[i].poblacion.length > 22)
                            poblaciones[i].poblacion = poblaciones[i].poblacion.substring(0, 21);
                        if (pobl == poblaciones[i].poblacion)
                            $("#poblacion").append("<option value='" + poblaciones[i].poblacion + "' selected='selected'>" + poblaciones[i].poblacion + "</option>");
                        else
                            $("#poblacion").append("<option value='" + poblaciones[i].poblacion + "'>" + poblaciones[i].poblacion + "</option>");
                    }
                }
            });
        }
    }]);