app.controller('adminCtrl', function () {

});

app.controller('listCtrl', function ($scope, user, $timeout) {
    $scope.messageFailure = "";
    $scope.profile_filler = {};

    $scope.list = user.data.user;
    $scope.currentPage = 1; //current page
    $scope.entryLimit = 5; //max no of items to display in a page
    $scope.numPerPage = 5;

    $scope.$watch('currentPage + numPerPage', update);

    function update() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;

        $scope.filteredlist = $scope.list.slice(begin, end);
    };
    
    $scope.sort_by = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});


