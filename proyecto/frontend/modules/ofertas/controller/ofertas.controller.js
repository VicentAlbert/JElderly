app.controller('ofertasCtrl', function ($scope, ofertas) {
    $scope.filteredOfertas = [];
    $scope.markers = [];
    $scope.oferta = ofertas.data.ofertas;
    $scope.numPerPage = 6;
    $scope.maxSize = 5;
    $scope.currentPage = 1;




    $scope.$watch('currentPage + numPerPage', update);



    cargarmap(ofertas.data.ofertas, $scope);

    $scope.select = function (id) {
        for (var i = 0; i < $scope.markers.length; i++) {
            var marker = $scope.markers[i];
            if (id == marker.get('id')) {
                if (marker.getAnimation() !== null) {
                    marker.setAnimation(null);
                } else {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                    $scope.map.setCenter(marker.latlon);
                }
                break;
            }
        }
    };

    function update() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;

        $scope.filteredOfertas = $scope.oferta.slice(begin, end);
    }
    ;

});

app.controller('detailsCtrl', function ($scope, data, services, CommonService, $cookies) {
    $scope.data = data.data.ofertas;
    $scope.stringAsistentes = $scope.data.asistentes;
    $scope.data.asistentes = $scope.stringAsistentes.split("|");
    $scope.UneteV = true;
    
    if ($cookies.getObject('session'))
        for (var i = 0; i < $scope.data.asistentes.length; i++)
            if ($scope.data.asistentes[i] === $cookies.getObject('session').usuario)
                $scope.UneteV = false;

    services.get('user', 'profile_filler', 3, data.data.ofertas.usuario).then(function (response) {
        if (!response.data.error) {
            $scope.usuario = response.data.user;
            $scope.usuario.rankYes = Number($scope.usuario.valoracion);
            $scope.usuario.rankNo = 5 - Number($scope.usuario.rankYes);

        }
    });
    $scope.getTimes = function (n) {
        return new Array(n);
    };
    $scope.join = function () {
        if ($cookies.getObject('session')) {
            var asis = $scope.stringAsistentes + "|" + $cookies.getObject('session').usuario;
            services.get('ofertas', 'join', 4, $scope.data.id, asis).then(function (response) {
                if (response.data.success) {
                    $scope.data.asistentes = response.data.datos.split("|");
                    $scope.UneteV = false;
                    window.location.href= window.location.href;
                } else {
                    CommonService.banner("Algo salió mal, inténtelo más tarde", "Err");
                }
            });
        } else {
            CommonService.banner("Haz login para poder unirte", "Err");
        }
    };
});

function cargarmap(arrArguments, $scope) {
    navigator.geolocation.getCurrentPosition(showPosition, showError);
    function showPosition(position)
    {
        lat = position.coords.latitude;
        lon = position.coords.longitude;
        latlon = new google.maps.LatLng(lat, lon);
        mapholder = document.getElementById('mapholder');
        //mapholder.style.height = '550px';
        //mapholder.style.width = '900px';
        var myOptions = {
            center: latlon, zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL}
        };
        var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);
        // var marker = new google.maps.Marker({position: latlon, map: map, title: "You are here!"});
        $scope.map = map;

        for (var i = 0; i < arrArguments.length; i++) {
            marcar(map, arrArguments[i], $scope);

        }
    }
    function showError(error)
    {
        switch (error.code)
        {
            case error.PERMISSION_DENIED:
                $scope.demo = "Denegada la peticion de Geolocalización en el navegador.";
                break;
            case error.POSITION_UNAVAILABLE:
                $scope.demo = "La información de la localización no esta disponible.";
                break;
            case error.TIMEOUT:
                $scope.demo = "El tiempo de petición ha expirado.";
                break;
            case error.UNKNOWN_ERROR:
                $scope.demo = "Ha ocurrido un error desconocido.";
                break;
        }
    }}
function marcar(map, oferta, $scope) {

    var latlon = new google.maps.LatLng(oferta.latitud, oferta.longitud);
    var marker = new google.maps.Marker({position: latlon, map: map, title: oferta.descripcion, animation: null});

    marker.set('id', oferta.id);
    marker.set('latlon', latlon);

    var infowindow = new google.maps.InfoWindow({
        content: '<h1 class="oferta_title">Oferta en ' + oferta.lugar_inicio + '</h1><p class="oferta_content">' + oferta.descripcion + '</p><p class="oferta_content">Día: ' + oferta.fecha_inicio + '</p><p class="oferta_content">Horario: ' + oferta.hora_inicio + ' - ' + oferta.hora_final + '</p>'
    });



    google.maps.event.addListener(marker, 'click', function () {

        infowindow.open(map, marker);



        google.maps.event.addListener(infowindow, 'domready', function () {


            var iwOuter = $('.gm-style-iw');

            var iwCloser = iwOuter.next();
            var iwBackground = iwOuter.prev();

            iwBackground.children(':nth-child(2)').css({'display': 'none'});


            iwBackground.children(':nth-child(4)').css({'display': 'none'});

            iwBackground.children(':nth-child(1)').attr('style', function (i, s) {
                return s + 'left: 76px !important;'
            });


            iwBackground.children(':nth-child(3)').attr('style', function (i, s) {
                return s + 'left: 76px !important;'
            });
            iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'background-color': '#f5f5f5', 'z-index': '1'});

            iwCloser.css({
                opacity: '1',
                right: '18px', top: '3px',
                'border-radius': '13px', // circular effect
                'box-shadow': '0 0 5px #3990B9' // 3D effect to highlight the button
            });
            iwCloser.mouseout(function () {
                $(this).css({opacity: '1'});
            });

        });
    });
    $scope.markers.push(marker);

}