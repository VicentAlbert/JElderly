app.factory("services", ['$http', function ($http) {
        var serviceBase = 'backend/index.php?module=';
        var obj = {};

        obj.get = function (module, functi, num, dada, dada2) {
            if (num == 2)
                return $http.get(serviceBase + module + '&function=' + functi);
            else if (num == 3)
                return $http.get(serviceBase + module + '&function=' + functi + '&param=' + dada);
            else if (num == 4)
                return $http.get(serviceBase + module + '&function=' + functi + '&param=' + dada + '&param2=' + dada2);
        };

        obj.post = function (module, functi, dada) {
            return $http.post(serviceBase + module + '&function=' + functi, dada);
            //return $http.post('app_model/index.php?module=customer&function=insertCustomer',customer);
        };

        obj.put = function (module, functi, dada) {
            return $http.put(serviceBase + module + '&function=' + functi, {'json': dada});
            //return $http.post('app_model/index.php?module=customer&function=updateCustomer',{id:id, customer:customer});
        };

        obj.delete = function (module, functi, dada) {
            return $http.delete(serviceBase + module + '&function=' + functi + '&param=' + dada);
            //return $http.delete('app_model/index.php?module=customer&function=deleteCustomer&id='+id);
        };

        return obj;
    }]);



