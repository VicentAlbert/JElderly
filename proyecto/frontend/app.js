var app = angular.module('myApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'ngCookies', 'facebook']);
//'ui.bootstrap' -> necesario para la paginación

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider

                // Home
                .when("/", {templateUrl: "frontend/modules/main/view/main.view.html", controller: "mainCtrl"})
                // Pages
                .when("/trabaja", {templateUrl: "frontend/modules/main/view/trabaja.view.html", controller: "mainCtrl"})
                .when("/aviso", {templateUrl: "frontend/modules/main/view/aviso.view.html", controller: "mainCtrl"})
                .when("/terminos", {templateUrl: "frontend/modules/main/view/terminos.view.html", controller: "mainCtrl"})
                .when("/politica", {templateUrl: "frontend/modules/main/view/politica.view.html", controller: "mainCtrl"})
                // Ofertas
                .when("/ofertas", {
                    templateUrl: "frontend/modules/ofertas/view/main.view.html",
                    controller: "ofertasCtrl",
                    resolve: {
                        ofertas: function (services) {
                            return services.get('ofertas', 'maploader', 2);
                        }
                    }
                })
                .when("/ofertas/:id", {
                    templateUrl: "frontend/modules/ofertas/view/oferta.view.html",
                    controller: "detailsCtrl",
                    resolve: {
                        data: function (services, $route) {
                            return services.get('ofertas', 'getOffer', 3, $route.current.params.id);

                        }
                    }
                })
                // Contact
                .when("/contact", {templateUrl: "frontend/modules/contact/view/contact.view.html", controller: "contactCtrl"})
                /********MODAL*********/
                //Restore
                .when("/user/recuperar", {
                    templateUrl: "frontend/modules/user/view/restore.view.html",
                    controller: "restoreCtrl"
                })
                //ChangePass
                .when("/user/cambiarpass/:token", {
                    templateUrl: "frontend/modules/user/view/changepass.view.html",
                    controller: "changepassCtrl"
                })
                //SignUP
                .when("/user/alta/", {
                    templateUrl: "frontend/modules/user/view/signup.view.html",
                    controller: "signupCtrl"
                })
                //Perfil
                .when("/user/profile/", {
                    templateUrl: "frontend/modules/user/view/profile.view.html",
                    controller: "profileCtrl",
                    resolve: {
                        user: function (services, $cookies) {
                            if ($cookies.getObject("session"))
                                return services.get('user', 'profile_filler', 3, $cookies.getObject("session").usuario);
                            return false;
                        }
                    }
                })
                //Activar Usuario
                .when("/user/activar/:token", {
                    templateUrl: "frontend/modules/main/view/main.view.html",
                    controller: "verifyCtrl"
                })
                 .when("/admin/", {
                    templateUrl: "frontend/modules/admin/view/admin.view.html",
                    controller: "adminCtrl"
                })
                .when("/admin/list", {
                    templateUrl: "frontend/modules/admin/view/adminUsers.view.html",
                    controller: "listCtrl",
                    resolve: {
                        user: function (services) {
                          
                                return services.get('user', 'profile_filler', 3, '%%');
                         
                        }
                    }
                })
                .when("/admin/edit/:username", {
                    templateUrl: "frontend/modules/user/view/profile.view.html",
                    controller: "profileCtrl",
                    resolve: {
                        user: function (services,$route) {
                          
                                return services.get('user', 'profile_filler', 3, $route.current.params.username);
                         
                        }
                    }
                })

                // else 404
                .otherwise("/", {templateUrl: "frontend/modules/main/view/main.view.html", controller: "mainCtrl"});
    }]);

app.config([
    'FacebookProvider',
    function (FacebookProvider) {
        var myAppId = '209655636048272';

        FacebookProvider.init(myAppId);
    }
]);

/* .run(function ($rootScope, $location, services) {
 $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
 $rootScope.title = current.$$route.title;
 });
 
 $rootScope.$on("$routeChangeStart", function (event, next, current) {
 $rootScope.authenticated = false;
 services.get('login', 'session')
 .then(function (results) {
 if (results.data.uid) {
 $rootScope.authenticated = true;
 $rootScope.uid = results.data.uid;
 $rootScope.name = results.data.name;
 $rootScope.email = results.data.email;
 }
 });
 });
 });*/


