CREATE DATABASE  IF NOT EXISTS `joinelderly` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `joinelderly`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: joinelderly
-- ------------------------------------------------------
-- Server version	5.5.44-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comentarios`
--

DROP TABLE IF EXISTS `comentarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL,
  `emisor` varchar(45) NOT NULL,
  `receptor` varchar(45) NOT NULL,
  `mensaje` varchar(100) NOT NULL,
  `fecha` varchar(45) NOT NULL,
  `hora` time NOT NULL,
  `valoracion` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentarios`
--

LOCK TABLES `comentarios` WRITE;
/*!40000 ALTER TABLE `comentarios` DISABLE KEYS */;
INSERT INTO `comentarios` VALUES (1,'lauramore','josegg85','Me ha gustado mucho la actividad que has realizado','10/06/2016','17:30:00',8),(2,'lauramore','miguelgh','Me hubiera gustado hacer la actividad durante mas tiempo','20/05/2016','19:08:00',7);
/*!40000 ALTER TABLE `comentarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ofertas`
--

DROP TABLE IF EXISTS `ofertas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ofertas` (
  `id` int(11) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `fecha_inicio` varchar(100) NOT NULL,
  `fecha_final` varchar(100) NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_final` time NOT NULL,
  `lugar_inicio` varchar(45) NOT NULL,
  `lugar_final` varchar(45) NOT NULL,
  `asistentes` varchar(200) NOT NULL,
  `precio` float NOT NULL,
  `latitud` float NOT NULL,
  `longitud` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ofertas`
--

LOCK TABLES `ofertas` WRITE;
/*!40000 ALTER TABLE `ofertas` DISABLE KEYS */;
INSERT INTO `ofertas` VALUES (1,'josegg85','Excursion a Tierra Natura','20/05/2016','20/05/2016','09:00:00','19:00:00','Valencia','Ontinyent','Paco&Miguel',25,39.4903,-0.393309),(2,'miguelgh','Visita al museo botanico','10/06/2016','10/06/2016','10:00:00','13:30:00','Valencia','Ontinyent','Jose&Laura',10,38.8175,-0.614507),(3,'josegg85','Partida de Petanca','15/02/2016','15/02/2016','09:30:00','12:00:00','Ontinyent','Ontinyent','Jose&Laura',2,38.8119,-0.610749),(4,'josegg85','Excursion al Bioparc','20/05/2016','20/05/2016','09:00:00','19:00:00','Valencia','Ontinyent','Paco&Miguel',25,39.4916,-0.401634),(5,'miguelgh','Visita al museo universal','10/06/2016','10/06/2016','10:00:00','13:30:00','Ontinyent','Ontinyent','Jose&Laura',10,38.8302,-0.579904),(6,'josegg85','Partida de Domino','15/02/2016','15/02/2016','09:30:00','12:00:00','Ontinyent','Ontinyent','Jose&Laura',2,38.8229,-0.550615),(7,'josegg85','Excursion al Pou Clar','20/05/2016','20/05/2016','09:00:00','19:00:00','Ontinyent','Ontinyent','Paco&Miguel',2,38.8105,-0.616876),(8,'miguelgh','Senderimos por la sierra','10/06/2016','10/06/2016','10:00:00','13:30:00','Xativa','Ontinyent','Jose&Laura',5,38.9897,-0.532869),(9,'josegg85','Partida de Parchis','15/02/2016','15/02/2016','09:30:00','12:00:00','Ontinyent','Ontinyent','Jose&Laura',2,38.819,-0.603822),(10,'josegg85','Excursion a la Warner','20/05/2016','20/05/2016','09:00:00','19:00:00','Ontinyent','Ontinyent','Paco&Miguel',19,38.8203,-0.605421),(11,'miguelgh','Visita a Valencia','10/06/2016','10/06/2016','10:00:00','13:30:00','Valencia','Ontinyent','Jose&Laura',20,38.8175,-0.614507),(12,'josegg85','Partida de Mus','15/02/2016','15/02/2016','09:30:00','12:00:00','Ontinyent','Ontinyent','Jose&Laura',3,38.8168,-0.608992),(13,'josegg85','Excursion a Aqualandia','20/05/2016','20/05/2016','09:00:00','19:00:00','Benidorm','Ontinyent','Paco&Miguel',25,38.5603,-0.133928),(14,'miguelgh','Visita a las Artes y las Ciencias','10/06/2016','10/06/2016','10:00:00','13:30:00','Valencia','Ontinyent','Jose&Laura',15,39.4571,-0.380863),(15,'josegg85','Partida de Cartas','15/02/2016','15/02/2016','09:30:00','12:00:00','Ontinyent','Ontinyent','Jose&Laura',1.5,38.8239,-0.59898);
/*!40000 ALTER TABLE `ofertas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `usuario` varchar(100) CHARACTER SET latin1 NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `apellidos` varchar(100) CHARACTER SET latin1 NOT NULL,
  `dni` varchar(9) CHARACTER SET latin1 NOT NULL,
  `password` varchar(200) CHARACTER SET latin1 NOT NULL,
  `date_birthday` varchar(45) CHARACTER SET latin1 NOT NULL,
  `tipo` varchar(45) CHARACTER SET latin1 NOT NULL,
  `bank` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pais` varchar(100) CHARACTER SET latin1 NOT NULL,
  `provincia` varchar(100) CHARACTER SET latin1 NOT NULL,
  `poblacion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `avatar` varchar(200) CHARACTER SET latin1 NOT NULL,
  `valoracion` float NOT NULL,
  `activado` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('','','','','','','','client','',' ',' ',' ','http://graph.facebook.com//picture',0,1,''),('158218454554356','joinelderly@gmail.com','JoinElderly','JE','','','','client','',' ',' ',' ','http://graph.facebook.com/158218454554356/picture',0,1,''),('4702104732','','JoinElderly','','','','','client','',' ',' ',' ','http://pbs.twimg.com/profile_images/686119679505596417/jE02HpCk_normal.png',0,1,''),('506299962','','Raul Colomer','','','','','client','',' ',' ',' ','http://pbs.twimg.com/profile_images/450282704500559872/VqVN6hpi_normal.jpeg',0,1,''),('665289310277342','vinche777@gmail.com','Vicent','Albert Borrell','','$2y$10$ZsSEiQKcdOf18cvBJ9hBX.nqWOv18WICQRRL606VPE.gHVZfssyM6','','client','',' ',' ',' ','http://graph.facebook.com/665289310277342/picture',0,1,''),('raul','raulcolomer_revert@hotmail.com','Raul','Colomer','48607190W','$2y$10$PQPH3clcNZsJ7D.JnuVtp.ryhQRLRyDCFWT4A1.rs3WyZSsVcOgTG','14/03/1993','worker','6589965899658','ES','46','Ontinyent','http://51.254.99.211/JoinElderly_project/proyecto/app_model/media/flowers.png',0,1,'Ver13c8940d2f04e0f4fdc73a6ba74b74d5'),('Vicent','vicent.albo@gmail.com','vicent','albert borrell','48607270J','$2y$10$.Q5mNS2UHGm2jVVu9aUVeOPMX3tIdh5EpMGuCS621pnKW.YRpCDdm','12/01/1995','client','1515424554','ES','33','Alburquerque','http://51.254.99.211/JoinElderly_project/proyecto/app_model/media/imggitVAB.png',0,1,'Ver14a40272b4c4a9f052d3f72fc00d2f8a');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'joinelderly'
--

--
-- Dumping routines for database 'joinelderly'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-21 12:07:56
